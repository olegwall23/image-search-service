package com.example.imagesearchservice;

import com.example.imagesearchservice.service.AuthService;
import com.example.imagesearchservice.service.ExternalImagesService;
import com.example.imagesearchservice.vo.ImagesFetchResponse;
import com.example.imagesearchservice.vo.Picture;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class PictureSearchServiceApplicationTests {

    @Autowired
    private AuthService authService;

    @Autowired
    private ExternalImagesService externalImagesService;

    @Test
    void authServiceTest() {
        String token = authService.getToken();
        assertTrue(token != null);
        String newToken = authService.renewToken();
        assertTrue(newToken != null && !token.equals(newToken));
    }

    @Test
    void externalImagesServiceTest() {
        ImagesFetchResponse imagesFetchResponse = externalImagesService.fetch(1);
        assertTrue(imagesFetchResponse.isHasMore() && imagesFetchResponse.getPictures().size() > 0);
        Picture picture = externalImagesService.getImageDetails(imagesFetchResponse.getPictures().get(0).getId());
        assertTrue(picture != null);
    }

}
