package com.example.imagesearchservice.scheduler;

import com.example.imagesearchservice.service.ExternalImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Component
public class ImageFetchScheduler {


    @PostConstruct
    private void init() {

    }

    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void reportCurrentTime() {

    }

}
