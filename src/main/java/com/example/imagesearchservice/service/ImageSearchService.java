package com.example.imagesearchservice.service;

import com.example.imagesearchservice.vo.Picture;

import java.util.List;

public interface ImageSearchService {

    void reindex(List<Picture> pictures);

    List<Picture> search(String searchTerm);

}
