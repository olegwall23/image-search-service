package com.example.imagesearchservice.service;

public interface AuthService {

    String getToken();

    String renewToken();

}
