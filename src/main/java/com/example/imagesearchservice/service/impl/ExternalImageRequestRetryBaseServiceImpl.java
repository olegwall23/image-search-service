package com.example.imagesearchservice.service.impl;

import com.example.imagesearchservice.service.AuthService;
import com.example.imagesearchservice.service.RequestRetryService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ExternalImageRequestRetryBaseServiceImpl implements RequestRetryService {

    @Autowired
    private AuthService authService;

    @Autowired
    private OkHttpClient client;

    private Logger logger = LoggerFactory.getLogger(ExternalImageRequestRetryBaseServiceImpl.class);

    @Override
    public String reqWithRetryAndTokenExpHandle(Request request, AuthService authService) {
        int retries = 0;
        int tokenRefreshTimes = 0;
        while (true) {
            Response response = null;
            try {
                response = client.newCall(request).execute();
                if (isUnauthorized(response)) {
                    if (tokenRefreshTimes == 3) {
                        logger.error("unauthorized after 3 token refresh");
                        return null;
                    }
                    authService.renewToken();
                    tokenRefreshTimes++;
                }
                if (response.code() == 200) {
                    String result = response.body().string();
                    return result;
                }
            } catch (IOException e) {
                e.printStackTrace();
                if (retries >= 3) {
                    logger.error("unable to request " + request.url() + " after " + retries);
                    return null;
                }
                logger.error("unable to request " + request.url() + " retry after 100ms");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                retries++;
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }
    }

    private boolean isUnauthorized(Response response) {
        if (response.code() == 401) {
            return true;
        }
        return false;
    }

}
