package com.example.imagesearchservice.service.impl;

import com.example.imagesearchservice.service.AuthService;
import com.example.imagesearchservice.service.ExternalImagesHelperService;
import com.example.imagesearchservice.service.ExternalImagesService;
import com.example.imagesearchservice.service.RequestRetryService;
import com.example.imagesearchservice.vo.ImagesFetchResponse;
import com.example.imagesearchservice.vo.Picture;
import okhttp3.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExternalImagesServiceImpl implements ExternalImagesService {

    @Autowired
    private AuthService authService;

    @Autowired
    private ExternalImagesHelperService externalImagesHelperService;

    @Autowired
    private RequestRetryService requestRetryService;

    private final String API_ENDPOINT = "http://interview.agileengine.com/images";

    private Logger logger = LoggerFactory.getLogger(ExternalImagesServiceImpl.class);

    @Override
    public ImagesFetchResponse fetch(int page) {
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + authService.getToken())
                .url(API_ENDPOINT + "?page=" + page)
                .build();

        String response = requestRetryService.reqWithRetryAndTokenExpHandle(request, authService);
        if (response != null) {
            return externalImagesHelperService.convertToImagesFetchResponse(response);
        }
        logger.error("unable to get result from " + (API_ENDPOINT + "?page=" + page));
        return null;
    }

    @Override
    public Picture getImageDetails(String imageId) {
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + authService.getToken())
                .url(API_ENDPOINT + "/" +imageId)
                .build();

        String response = requestRetryService.reqWithRetryAndTokenExpHandle(request, authService);
        if (response != null) {
            return externalImagesHelperService.convertToPicture(response);
        }
        logger.error("unable to get result from " + (API_ENDPOINT + "/" + imageId));
        return null;
    }

}
