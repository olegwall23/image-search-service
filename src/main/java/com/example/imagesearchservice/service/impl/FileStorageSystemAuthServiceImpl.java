package com.example.imagesearchservice.service.impl;

import com.example.imagesearchservice.service.AuthService;
import okhttp3.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class FileStorageSystemAuthServiceImpl implements AuthService {

    private String token;

    @Autowired
    private OkHttpClient client;

    private final String API_KEY = "23567b218376f79d9415";
    private final String API_ENDPOINT = "http://interview.agileengine.com/auth";

    @PostConstruct
    private void init() {
        renewToken();
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public synchronized String renewToken() {
        RequestBody body = RequestBody.create(getRequestBody(), MediaType.get("application/json; charset=utf-8"));
        Request request = new Request.Builder()
                .url(API_ENDPOINT)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            JSONObject result = new JSONObject(response.body().string());
            if (result.has("token")) {
                token = result.getString("token");
                return token;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return token = null;
    }

    private String getRequestBody() {
        return "{ \"apiKey\": \"" + API_KEY + "\" }";
    }

}
