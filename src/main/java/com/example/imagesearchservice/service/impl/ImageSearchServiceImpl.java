package com.example.imagesearchservice.service.impl;

import com.example.imagesearchservice.service.ImageSearchService;
import com.example.imagesearchservice.vo.Picture;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ImageSearchServiceImpl implements ImageSearchService {

    private Map<String, List<String>> oldPictureTermsByPictureId = new HashMap<>();
    private Map<String, Set<Picture>> picturesByTermSearch = new HashMap<>();

    private List<String> availableSearchTerms = Arrays.asList("id", "author", "camera", "tag");

    private final Object lock = new Object();

    @Override
    public void reindex(List<Picture> pictures) {
        pictures.forEach(this::reindex);
    }

    private void reindex(Picture picture) {
        removeFromIndex(picture);
        index(picture);
    }

    private void index(Picture picture) {
        synchronized (lock) {
            List<String> searchTermsForIndex = getSearchTerm(picture);
            searchTermsForIndex.forEach(term -> {
                Set<Picture> picturesList = picturesByTermSearch.get(term);
                if (picturesList == null) {
                    picturesList = new HashSet<>();
                    picturesList.add(picture);
                    picturesByTermSearch.put(term, picturesList);
                } else {
                    picturesList.add(picture);
                }
            });
            oldPictureTermsByPictureId.put(picture.getId(), searchTermsForIndex);
        }
    }

    private void removeFromIndex(Picture picture) {
        synchronized (lock) {
            List<String> searchTermsForIndex = oldPictureTermsByPictureId.get(picture.getId());
            searchTermsForIndex.forEach(term -> {
                Set<Picture> picturesList = picturesByTermSearch.get(term);
                if (picturesList != null) {
                    picturesList.remove(picture);
                }
            });
        }
    }

    @Override
    public List<Picture> search(String searchTerm) {
        synchronized (lock) {
            Set<Picture> result = new HashSet<>();
            for (String term : availableSearchTerms) {
                result.addAll(picturesByTermSearch.get(term + "|" + searchTerm));
            }
            return new ArrayList<>(result);
        }
    }

    private List<String> getSearchTerm(Picture picture) {
        List<String> searchTermsForPicture = new ArrayList<>();
        searchTermsForPicture.add("id|" + picture.getId());
        searchTermsForPicture.add("author|" + picture.getAuthor());
        searchTermsForPicture.add("camera|" + picture.getCamera());
        for (String tag : picture.getTags().split(" ")) {
            searchTermsForPicture.add("tag|" + tag);
        }
        return searchTermsForPicture;
    }

}
