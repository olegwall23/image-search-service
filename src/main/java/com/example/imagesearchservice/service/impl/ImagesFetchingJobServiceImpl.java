package com.example.imagesearchservice.service.impl;

import com.example.imagesearchservice.service.ExternalImagesHelperService;
import com.example.imagesearchservice.service.ExternalImagesService;
import com.example.imagesearchservice.service.ImageSearchService;
import com.example.imagesearchservice.service.ImagesFetchingJobService;
import com.example.imagesearchservice.vo.ImagesFetchResponse;
import com.example.imagesearchservice.vo.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ImagesFetchingJobServiceImpl implements ImagesFetchingJobService {

    @Autowired
    private ExternalImagesService externalImagesService;

    @Autowired
    private ExternalImagesHelperService externalImagesHelperService;

    @Autowired
    private ImageSearchService imageSearchService;

    public void fetchImages() {
        int currentPage = 1;
        while (true) {
            ImagesFetchResponse imagesFetchResponse = externalImagesService.fetch(currentPage);
            externalImagesHelperService.addPictureDetailsToResponse(imagesFetchResponse, fetchPictureDetails(imagesFetchResponse));

            imageSearchService.reindex(imagesFetchResponse.getPictures());

            if (!imagesFetchResponse.isHasMore()) {
                break;
            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            currentPage++;
        }
    }

    private List<Picture> fetchPictureDetails(ImagesFetchResponse imagesFetchResponse) {
        return imagesFetchResponse.getPictures().stream().map(Picture::getId)
                .map(externalImagesService::getImageDetails).filter(Objects::nonNull).collect(Collectors.toList());
    }

}
