package com.example.imagesearchservice.service.impl;

import com.example.imagesearchservice.service.ExternalImagesHelperService;
import com.example.imagesearchservice.vo.ImagesFetchResponse;
import com.example.imagesearchservice.vo.Picture;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExternalImagesHelperServiceImpl implements ExternalImagesHelperService {

    @Override
    public ImagesFetchResponse convertToImagesFetchResponse(String jsonData) {
        JSONObject json = new JSONObject(jsonData);
        int page = json.getInt("page");
        int pageCount = json.getInt("pageCount");
        boolean hasMore = json.getBoolean("hasMore");

        JSONArray pictures = json.getJSONArray("pictures");
        List<Picture> pictureList = new ArrayList<>(pictures.length());
        for (int i = 0; i < pictures.length(); i++) {
            JSONObject picture = pictures.getJSONObject(i);
            pictureList.add(Picture.builder()
                    .id(picture.getString("id"))
                    .croppedPicture(picture.getString("cropped_picture"))
                    .build());
        }

        return ImagesFetchResponse.builder()
                .pictures(pictureList)
                .page(page)
                .pageCount(pageCount)
                .hasMore(hasMore)
                .build();
    }

    @Override
    public Picture convertToPicture(String jsonData) {
        JSONObject jsonObject = new JSONObject(jsonData);
        return Picture.builder()
                .id(jsonObject.getString("id"))
                .author(jsonObject.getString("author"))
                .camera(jsonObject.getString("camera"))
                .tags(jsonObject.getString("tags"))
                .croppedPicture(jsonObject.getString("cropped_picture"))
                .fullPicture(jsonObject.getString("full_picture"))
                .build();
    }

    @Override
    public ImagesFetchResponse addPictureDetailsToResponse(ImagesFetchResponse imagesFetchResponse, List<Picture> pictures) {
        Map<String, Picture> pictureMap = new HashMap<>(imagesFetchResponse.getPictures().size());
        imagesFetchResponse.getPictures().forEach(pic -> {
            pictureMap.put(pic.getId(), pic);
        });
        pictures.forEach(pic -> {
            pictureMap.put(pic.getId(), pic);
        });
        imagesFetchResponse.setPictures(new ArrayList<>(pictureMap.values()));
        return imagesFetchResponse;
    }

}
