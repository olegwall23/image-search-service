package com.example.imagesearchservice.service;

import com.example.imagesearchservice.vo.ImagesFetchResponse;
import com.example.imagesearchservice.vo.Picture;

import java.util.List;

public interface ExternalImagesHelperService {

    ImagesFetchResponse convertToImagesFetchResponse(String jsonData);

    Picture convertToPicture(String jsonData);

    ImagesFetchResponse addPictureDetailsToResponse(ImagesFetchResponse imagesFetchResponse, List<Picture> pictures);

}
