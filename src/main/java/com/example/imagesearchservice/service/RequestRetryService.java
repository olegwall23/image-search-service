package com.example.imagesearchservice.service;

import okhttp3.Request;
import okhttp3.Response;

public interface RequestRetryService {

    String reqWithRetryAndTokenExpHandle(Request request, AuthService authService);

}
