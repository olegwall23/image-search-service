package com.example.imagesearchservice.service;

import com.example.imagesearchservice.vo.ImagesFetchResponse;
import com.example.imagesearchservice.vo.Picture;

public interface ExternalImagesService {

    ImagesFetchResponse fetch(int page);

    Picture getImageDetails(String imageId);

}
