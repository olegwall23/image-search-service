package com.example.imagesearchservice.controller;

import com.example.imagesearchservice.service.ImageSearchService;
import com.example.imagesearchservice.vo.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ImageSearchController {

    @Autowired
    private ImageSearchService imageSearchService;

    @GetMapping("/search/{searchTerm}")
    public List<Picture> search(@PathVariable String searchTerm) {
        return imageSearchService.search(searchTerm);
    }

}
