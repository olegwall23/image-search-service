package com.example.imagesearchservice.vo;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ImagesFetchResponse {
    private List<Picture> pictures;
    private int page;
    private int pageCount;
    private boolean hasMore;
}
