package com.example.imagesearchservice.config;

import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HttpClient {
    @Bean
    public OkHttpClient client() {
        return new OkHttpClient();
    }
}
