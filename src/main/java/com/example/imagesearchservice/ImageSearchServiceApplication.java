package com.example.imagesearchservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class ImageSearchServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImageSearchServiceApplication.class, args);
    }

}
